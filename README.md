# Lenador System Test

## Overview

Greetings! This is my submission for the Lenador System Test. The Android application showcases key features related to product management, order processing, and order history.

## Features

### 1. Adding Product

- Implemented a seamless interface for users to add products to the Room database.

### 2. Adding Order

- Developed a robust order creation process, allowing users to add products to their orders.
- Implemented dynamic calculations to determine the total value of each order.

### 3. Review Old Orders

- Created an Orders screen to provide users with a convenient overview of their past orders.
- Ensured a user-friendly experience by organizing and presenting order history effectively.

### 4. Change Tax Type

- Implemented a setting screen with a toggle option for users to switch between tax-inclusive and tax-exclusive pricing.
- Integrated internet-sourced tax formulas to ensure accurate calculations.

## Result Submission

Please feel free to explore the codebase, and I am available for any questions or clarifications.

Happy reviewing!
