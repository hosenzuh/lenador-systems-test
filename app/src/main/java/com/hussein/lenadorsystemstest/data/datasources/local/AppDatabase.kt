package com.hussein.lenadorsystemstest.data.datasources.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.Product
import com.hussein.lenadorsystemstest.model.ProductItem

@Database(
    entities = [Product::class, Order::class, ProductItem::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun productsDao(): ProductsDao

    abstract fun ordersDao(): OrdersDao
}