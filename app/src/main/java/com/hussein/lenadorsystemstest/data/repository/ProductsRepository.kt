package com.hussein.lenadorsystemstest.data.repository

import com.hussein.lenadorsystemstest.data.datasources.local.OrdersDao
import com.hussein.lenadorsystemstest.data.datasources.local.ProductsDao
import com.hussein.lenadorsystemstest.data.repository.base.BaseRepository
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.model.Product
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductsRepository @Inject constructor(
    private val productsDao: ProductsDao
) : BaseRepository() {

    suspend fun insertProduct(name: String, price: Double) = insertItem {
        productsDao.insertProduct(Product(name = name, price = price))
    }

    fun getAllProducts() =
        productsDao.getAllProducts().map<List<Product>, State<List<Product>>> {
            State.success(it)
        }

}