package com.hussein.lenadorsystemstest.data.datasources.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Upsert
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.OrderProducts
import com.hussein.lenadorsystemstest.model.ProductItem
import kotlinx.coroutines.flow.Flow

@Dao
interface OrdersDao {

    @Insert
    suspend fun insertOrder(order: Order): Long

    @Insert
    suspend fun insertProductsToACart(vararg productItems: ProductItem)

    @Transaction
    suspend fun insertOrderWithItsProducts(order: Order, vararg productItems: ProductItem) {
        val orderId = insertOrder(order)
        val updatedProductsOrderId = productItems.map { it.copy(orderId = orderId.toInt()) }
        insertProductsToACart(*updatedProductsOrderId.toTypedArray())
    }

    @Transaction
    @Query("SELECT * FROM `Order`")
    fun getAllOrders(): Flow<List<OrderProducts>>

}