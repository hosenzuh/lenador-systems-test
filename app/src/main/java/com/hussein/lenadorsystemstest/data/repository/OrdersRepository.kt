package com.hussein.lenadorsystemstest.data.repository

import com.hussein.lenadorsystemstest.data.datasources.local.OrdersDao
import com.hussein.lenadorsystemstest.data.datasources.local.ProductsDao
import com.hussein.lenadorsystemstest.data.repository.base.BaseRepository
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.OrderProducts
import com.hussein.lenadorsystemstest.model.OrderState
import com.hussein.lenadorsystemstest.model.Product
import com.hussein.lenadorsystemstest.model.ProductItem
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrdersRepository @Inject constructor(
    private val ordersDao: OrdersDao
) : BaseRepository() {

    suspend fun insertOrder(
        order: Order,
        productItems: List<ProductItem>
    ) = insertItem {
        ordersDao.insertOrderWithItsProducts(order, *productItems.toTypedArray())
    }

    fun getAllOrders() = ordersDao.getAllOrders().map<_, State<List<OrderProducts>>> {
        State.success(it)
    }

}