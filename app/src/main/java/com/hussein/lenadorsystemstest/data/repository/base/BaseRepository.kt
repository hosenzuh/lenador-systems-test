package com.hussein.lenadorsystemstest.data.repository.base

import com.hussein.lenadorsystemstest.utils.logcat
import com.hussein.lenadorsystemstest.domain.base.State
import java.util.concurrent.CancellationException

abstract class BaseRepository {


    suspend fun insertItem(insertionRequest: suspend () -> Unit): State<Unit> {
        return try {
            insertionRequest()
            State.success()
        } catch (exception: Exception) {
            logcat(exception)

            if (exception is CancellationException) throw exception

            State.failure()
        }
    }

}