package com.hussein.lenadorsystemstest.data.datasources.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Upsert
import com.hussein.lenadorsystemstest.model.Product
import com.hussein.lenadorsystemstest.model.ProductItem
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductsDao {

    @Insert
    suspend fun insertProduct(product: Product)

    @Query("SELECT * FROM Product")
    fun getAllProducts(): Flow<List<Product>>

}