package com.hussein.lenadorsystemstest.domain.order

import com.hussein.lenadorsystemstest.utils.logcat
import com.hussein.lenadorsystemstest.data.repository.OrdersRepository
import com.hussein.lenadorsystemstest.domain.base.State
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

class GetAllOrdersUseCase @Inject constructor(
    private val ordersRepository: OrdersRepository
) {

    operator fun invoke() = ordersRepository.getAllOrders().catch {
        logcat(it)

        emit(State.failure())
    }
}