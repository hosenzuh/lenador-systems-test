package com.hussein.lenadorsystemstest.domain.product

import com.hussein.lenadorsystemstest.utils.logcat
import com.hussein.lenadorsystemstest.data.repository.ProductsRepository
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.domain.base.State.Failure.InputInvalid.Companion.InvalidInputsTypes
import com.hussein.lenadorsystemstest.domain.base.UseCase
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AddProductUseCase @Inject constructor(
    private val productsRepository: ProductsRepository
) : UseCase() {

    operator fun invoke(name: String, price: String) = flow<State<Unit>> {
        val invalidName = name.isEmpty()
        val invalidPrice =
            price.isEmpty() || price.toDoubleOrNull() == null || price.toDouble() < 0.0
        if (invalidName || invalidPrice) {
            val invalidInputsTypes = arrayOf(invalidName.takeIf { it }
                ?.let { InvalidInputsTypes.NAME },
                invalidPrice.takeIf { it }?.let { InvalidInputsTypes.PRICE })
                .filterNotNull()
            emit(
                State.Failure.InputInvalid(
                    message = null,
                    invalidInputsTypes
                )
            )
            return@flow
        }
        emit(State.loading())

        emit(productsRepository.insertProduct(name, price.toDouble()))
    }.catch {
        logcat(it)

        emit(State.failure())
    }
}