package com.hussein.lenadorsystemstest.domain.base

import com.hussein.lenadorsystemstest.utils.logcat
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

abstract class UseCase

abstract class ReceiveDataUseCase<T>(
) : UseCase() {

    protected fun execute(request: suspend () -> State<T>) = flow {

        emit(State.loading())
        emit(request())
    }.catch { throwable ->
        logcat(throwable)

        emit(State.failure())
    }
}

abstract class SendDataUseCase(
) : UseCase() {

    protected fun execute(request: suspend () -> State<Unit>) = flow {

        emit(State.loading())
        emit(request())
    }.catch { throwable ->
        logcat(throwable)

        emit(State.failure())
    }
}