package com.hussein.lenadorsystemstest.domain.order

import com.hussein.lenadorsystemstest.utils.logcat
import com.hussein.lenadorsystemstest.data.repository.OrdersRepository
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.domain.base.UseCase
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.ProductItem
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AddOrderUseCase @Inject constructor(
    private val ordersRepository: OrdersRepository
) : UseCase() {

    operator fun invoke(order: Order, productItems: List<ProductItem>) = flow<State<Unit>> {
        emit(State.loading())

        emit(ordersRepository.insertOrder(order, productItems))
    }.catch {
        logcat(it)

        emit(State.failure())
    }
}