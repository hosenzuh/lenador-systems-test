package com.hussein.lenadorsystemstest.domain.product

import com.hussein.lenadorsystemstest.utils.logcat
import com.hussein.lenadorsystemstest.data.repository.ProductsRepository
import com.hussein.lenadorsystemstest.domain.base.State
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

class GetAllProductsUseCase @Inject constructor(
    private val productsRepository: ProductsRepository
) {

    operator fun invoke() = productsRepository.getAllProducts().catch {
        logcat(it)

        emit(State.failure())
    }
}