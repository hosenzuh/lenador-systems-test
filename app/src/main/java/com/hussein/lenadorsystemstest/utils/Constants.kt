package com.hussein.lenadorsystemstest.utils

const val SHARED_PREFERENCES_KEY = "SharedPreferences"
const val TAX_TYPE_KEY = "TaxType"


enum class TaxType {
    INCLUSIVE,
    EXCLUSIVE
}

var currentTaxType = TaxType.EXCLUSIVE

// We suppose this is fixed, but we can add the ability to change it from the UI if we wanted.
const val taxPercentage = 0.25