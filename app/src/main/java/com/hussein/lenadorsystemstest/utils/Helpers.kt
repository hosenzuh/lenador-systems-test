package com.hussein.lenadorsystemstest.utils

import android.util.Log

const val LOG_TAG = "Compose Playground"

fun logcat(throwable: Throwable, message: String = "") {
    Log.e(LOG_TAG, message, throwable)
}

fun calculateInclusiveTax(price: Double, taxRate: Double): Double {
    val inclusivePrice = price / (1 + taxRate)
    return price - inclusivePrice
}

fun calculateExclusiveTax(price: Double, taxRate: Double): Double {
    return price * taxRate
}