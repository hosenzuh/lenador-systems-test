package com.hussein.lenadorsystemstest.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.Relation
import com.hussein.lenadorsystemstest.utils.TaxType
import com.hussein.lenadorsystemstest.utils.calculateExclusiveTax
import com.hussein.lenadorsystemstest.utils.calculateInclusiveTax
import com.hussein.lenadorsystemstest.utils.taxPercentage

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Product::class,
            parentColumns = ["id"],
            childColumns = ["productId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = Order::class,
            parentColumns = ["id"],
            childColumns = ["orderId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class ProductItem(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val orderId: Int,
    val productId: Int,
    val name: String,
    val price: Double,
    val quantity: Int,
    val discount: Double,
    val taxType: TaxType,
) {
    @Ignore
    val subtotal = price * quantity

    @Ignore
    val totalTax = when (taxType) {
        TaxType.INCLUSIVE -> calculateInclusiveTax(subtotal, taxPercentage)
        TaxType.EXCLUSIVE -> calculateExclusiveTax(subtotal, taxPercentage)
    }

    @Ignore
    val totalPrice = when (taxType) {
        TaxType.INCLUSIVE -> subtotal
        TaxType.EXCLUSIVE -> subtotal + totalTax
    }

}