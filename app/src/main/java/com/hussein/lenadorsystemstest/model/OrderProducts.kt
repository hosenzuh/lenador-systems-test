package com.hussein.lenadorsystemstest.model

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import com.hussein.lenadorsystemstest.utils.TaxType
import com.hussein.lenadorsystemstest.utils.calculateExclusiveTax
import com.hussein.lenadorsystemstest.utils.calculateInclusiveTax
import com.hussein.lenadorsystemstest.utils.taxPercentage

data class OrderProducts(
    @Embedded val order: Order,
    @Relation(
        entity = ProductItem::class,
        parentColumn = "id",
        entityColumn = "orderId",
    ) val productItems: List<ProductItem>,
) {
    @Ignore
    val subtotalAmount = productItems.sumOf { it.subtotal }

    @Ignore
    val totalTax = when (order.taxType) {
        TaxType.INCLUSIVE -> calculateInclusiveTax(subtotalAmount, taxPercentage)
        TaxType.EXCLUSIVE -> calculateExclusiveTax(subtotalAmount, taxPercentage)
    }

    @Ignore
    val totalAmount = when (order.taxType) {
        TaxType.INCLUSIVE -> subtotalAmount
        TaxType.EXCLUSIVE -> subtotalAmount + totalTax
    }

    @Ignore
    val productsCount = productItems.size

    @Ignore
    val totalQuantity = productItems.sumOf { it.quantity }

}