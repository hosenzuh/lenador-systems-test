package com.hussein.lenadorsystemstest.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hussein.lenadorsystemstest.utils.TaxType

@Entity
data class Order(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val creationDate: String,
    val state: OrderState,
    val taxType: TaxType
)

enum class OrderState {
    Paid,
    Suspended,
    Voided,
    NONE,
}