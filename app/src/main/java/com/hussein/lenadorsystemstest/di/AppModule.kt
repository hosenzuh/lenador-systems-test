package com.hussein.lenadorsystemstest.di

import android.content.Context
import androidx.room.Room
import com.hussein.lenadorsystemstest.data.datasources.local.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module()
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val DATABASE_NAME =  "AppDatabase"

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context:Context) = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        DATABASE_NAME,
    ).build()

    @Provides
    @Singleton
    fun provideProductsDao(appDatabase: AppDatabase) = appDatabase.productsDao()

    @Provides
    @Singleton
    fun provideOrdersDao(appDatabase: AppDatabase) = appDatabase.ordersDao()


}