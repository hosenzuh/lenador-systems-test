package com.hussein.lenadorsystemstest.ui.main

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.ui.home.HomeScreen
import com.hussein.lenadorsystemstest.ui.order.allOrders.AllOrdersScreen
import com.hussein.lenadorsystemstest.ui.order.allOrders.AllOrdersViewModel
import com.hussein.lenadorsystemstest.ui.order.newOrder.NewOrderScreen
import com.hussein.lenadorsystemstest.ui.order.newOrder.NewOrderViewModel
import com.hussein.lenadorsystemstest.ui.theme.LenadorSystemsTestTheme
import com.hussein.lenadorsystemstest.utils.SHARED_PREFERENCES_KEY
import com.hussein.lenadorsystemstest.utils.TAX_TYPE_KEY
import com.hussein.lenadorsystemstest.utils.TaxType
import com.hussein.lenadorsystemstest.utils.currentTaxType
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentTaxType = getSharedPreferences(
            SHARED_PREFERENCES_KEY,
            MODE_PRIVATE
        ).getBoolean(TAX_TYPE_KEY, false).let {
            if (it) TaxType.INCLUSIVE
            else TaxType.EXCLUSIVE
        }
        setContent {
            LenadorSystemsTestTheme {
                Main()
            }
        }
    }
}

@Composable
fun Main() {
    val navController = rememberNavController()
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        NavHost(navController = navController, startDestination = NavDestination.Home.route) {

            composable(NavDestination.Home.route) {
                HomeScreen(
                    onNewOrderClicked = { navController.navigate(NavDestination.NewOrder.route) },
                    onAllOrdersClicked = { navController.navigate(NavDestination.AllOrders.route) },
                )
            }

            composable(NavDestination.NewOrder.route) {
                val viewModel = hiltViewModel<NewOrderViewModel>()
                val productsState by viewModel.productsFlow.collectAsStateWithLifecycle(initialValue = State.initial())
                val tempOrderProducts by viewModel.tempOrderProducts.collectAsStateWithLifecycle()
                val coroutine = rememberCoroutineScope()
                val context = LocalContext.current
                NewOrderScreen(
                    productsState,
                    tempOrderProducts,
                    onCancelClicked = {
                        navController.navigateUp()
                    },
                    onAddProductClicked = viewModel::addProduct,
                    onPayClicked = {
                        coroutine.launch {
                            viewModel.addOrder().collect {
                                if (it is State.Success) {
                                    Toast.makeText(
                                        context,
                                        "Added Successfully",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    navController.navigateUp()
                                } else if (it is State.Failure) {
                                    Toast.makeText(
                                        context,
                                        "An Error Occurred ${it.message ?: ""}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    navController.navigateUp()
                                }
                            }
                        }
                    },
                    onSuspendClicked = {
                        coroutine.launch {
                            viewModel.suspendOrder().collect {
                                if (it is State.Success) {
                                    Toast.makeText(
                                        context,
                                        "Suspended Successfully",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    navController.navigateUp()
                                } else if (it is State.Failure) {
                                    Toast.makeText(
                                        context,
                                        "An Error Occurred ${it.message ?: ""}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    navController.navigateUp()
                                }
                            }
                        }
                    },
                    onProductClicked = viewModel::onProductClicked,
                    onDeleteProductClicked = viewModel::onDeleteProductClicked,
                    onClearClicked = viewModel::onClearClicked
                )
            }

            composable(NavDestination.AllOrders.route) {
                val allOrderViewModel = hiltViewModel<AllOrdersViewModel>()
                val allOrdersState by allOrderViewModel.allOrdersFlow.collectAsStateWithLifecycle(
                    initialValue = State.initial()
                )
                AllOrdersScreen(allOrdersState)
            }

        }
    }
}

@PreviewLightDark
@Composable
fun GreetingPreview() {
    LenadorSystemsTestTheme {
        Main()
    }
}