package com.hussein.lenadorsystemstest.ui.order.newOrder

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Paid
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.Percent
import androidx.compose.material.icons.filled.Print
import androidx.compose.material3.Card
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewScreenSizes
import androidx.compose.ui.unit.dp
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.OrderProducts
import com.hussein.lenadorsystemstest.model.OrderState
import com.hussein.lenadorsystemstest.model.Product
import com.hussein.lenadorsystemstest.model.ProductItem
import com.hussein.lenadorsystemstest.ui.theme.Default
import com.hussein.lenadorsystemstest.ui.theme.HalfDefault
import com.hussein.lenadorsystemstest.ui.theme.LenadorSystemsTestTheme
import com.hussein.lenadorsystemstest.ui.theme.QuarterDefault
import com.hussein.lenadorsystemstest.utils.TaxType
import com.hussein.lenadorsystemstest.utils.toStringWith2DigitsWithAED
import com.sunnychung.lib.android.composabletable.ux.Table
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlin.reflect.KFunction0


@Composable
fun NewOrderScreen(
    products: State<List<Product>>,
    tempOrderProducts: OrderProducts,
    onCancelClicked: () -> Unit,
    onAddProductClicked: (name: String, price: String) -> Flow<State<Unit>>,
    onPayClicked: () -> Unit,
    onSuspendClicked: () -> Unit,
    onProductClicked: (product: Product) -> Unit,
    onDeleteProductClicked: (ProductItem) -> Unit,
    modifier: Modifier = Modifier,
    onClearClicked: KFunction0<Unit>,
) {
    val isAddProductVisible = remember {
        mutableStateOf(false)
    }
    Box {
        Row(modifier.fillMaxSize()) {
            Column(Modifier.weight(2f)) {
                OrderProductsTable(
                    Modifier.weight(1f),
                    tempOrderProducts.productItems,
                    onDeleteProductClicked
                )
                OrderControlPanel(
                    modifier = Modifier,
                    tempOrderProducts,
                    onCancelClicked,
                    onClearClicked,
                    onPayClicked,
                    onSuspendClicked,
                )
                Spacer(modifier = Modifier.height(QuarterDefault))
            }

            ProductsControlPanel(
                Modifier.weight(1f),
                (products as? State.Success.Data<List<Product>>)?.data ?: emptyList(),
                onAddProductClicked = {
                    isAddProductVisible.value = true
                },
                onProductClicked = onProductClicked
            )
        }
        AnimatedVisibility(
            visible = isAddProductVisible.value,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            AddingProductDialog(onAddProductClicked, isAddProductVisible)
        }
    }
}

@Composable
private fun AddingProductDialog(
    onAddProductClicked: (name: String, price: String) -> Flow<State<Unit>>,
    isAddProductVisible: MutableState<Boolean>
) {
    BackHandler {
        isAddProductVisible.value = false
    }
    var name: String by mutableStateOf("")
    var price: String by mutableStateOf("")

    val coroutine = rememberCoroutineScope()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                Color.LightGray.copy(alpha = 0.5f)
            )
            .clickable {
                isAddProductVisible.value = false
            }
    ) {
        ElevatedCard(
            Modifier
                .fillMaxWidth(0.5f)
                .align(Alignment.Center)
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(Default),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceEvenly
            ) {

                var isNameFieldContainsError by remember {
                    mutableStateOf(false)
                }
                var isPriceFieldContainsError by remember {
                    mutableStateOf(false)
                }

                Text(text = "Add New Product")
                TextField(
                    value = name, onValueChange = {
                        name = it
                        isNameFieldContainsError = false
                    },
                    placeholder = { Text(text = "Name") },
                    singleLine = true,
                    isError = isNameFieldContainsError,
                    supportingText = {
                        if (isNameFieldContainsError) {
                            Text(text = "Please fill this field")
                        }
                    }
                )
                TextField(
                    value = price,
                    onValueChange = {
                        price = it
                        isPriceFieldContainsError = false
                    },
                    placeholder = { Text(text = "Price") },
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Decimal),
                    singleLine = true,
                    isError = isPriceFieldContainsError,
                    supportingText = {
                        if (isPriceFieldContainsError) {
                            Text(text = "Please fill this field in positive number")
                        }
                    }
                )

                val context = LocalContext.current
                ElevatedButton(onClick = {
                    coroutine.launch {
                        onAddProductClicked(name, price).collect {
                            if (it is State.Success) {
                                Toast.makeText(
                                    context,
                                    "Added Successfully",
                                    Toast.LENGTH_SHORT
                                ).show()
                                isAddProductVisible.value = false
                            } else if (it is State.Failure.InputInvalid) {
                                it.invalidInputs.forEach {
                                    when (it) {
                                        State.Failure.InputInvalid.Companion.InvalidInputsTypes.NAME -> {
                                            isNameFieldContainsError = true
                                        }

                                        State.Failure.InputInvalid.Companion.InvalidInputsTypes.PRICE -> {
                                            isPriceFieldContainsError = true
                                        }
                                    }
                                }
                            } else if (it is State.Failure) {
                                Toast.makeText(
                                    context,
                                    "An Error Occurred ${it.message ?: ""}",
                                    Toast.LENGTH_SHORT
                                ).show()
                                isAddProductVisible.value = false
                            }
                        }
                    }
                }) {
                    Text(text = "Save")
                }
            }
        }
    }
}

@Composable
fun OrderProductsTable(
    modifier: Modifier = Modifier,
    tempProductItems: List<ProductItem>,
    onDeleteProductClicked: (ProductItem) -> Unit
) {

    Table(
        modifier = modifier.fillMaxSize(), columnCount = 7, rowCount = tempProductItems.size + 1,

        ) { rowIndex, columnIndex ->
        Box {

            when (rowIndex) {
                0 -> {
                    when (columnIndex) {
                        0 -> HeaderCell(text = "Name")
                        1 -> HeaderCell(text = "Price")
                        2 -> HeaderCell(text = "Tax")
                        3 -> HeaderCell(text = "Discount")
                        4 -> HeaderCell(text = "Quantity")
                        5 -> HeaderCell(text = "Total")
                        6 -> IconButton(onClick = { }) {
                            Icon(
                                imageVector = Icons.Default.Delete,
                                contentDescription = "",
                                tint = Color.Transparent
                            )
                        }
                    }
                }

                else -> {
                    if (tempProductItems.isEmpty()) return@Table
                    val rowItem = tempProductItems[rowIndex - 1]
                    with(rowItem) {
                        when (columnIndex) {
                            0 -> ContentCell(text = name)
                            1 -> ContentCell(text = price.toStringWith2DigitsWithAED())
                            2 -> ContentCell(text = totalTax.toStringWith2DigitsWithAED())
                            3 -> ContentCell(text = discount.toStringWith2DigitsWithAED())
                            4 -> ContentCell(text = quantity.toString())
                            5 -> ContentCell(text = totalPrice.toStringWith2DigitsWithAED())
                            6 -> IconButton(onClick = { onDeleteProductClicked(this) }) {
                                Icon(
                                    imageVector = Icons.Default.Delete,
                                    contentDescription = "Delete product from order",
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun BoxScope.ContentCell(text: String) {
    Text(
        text = text,
        modifier = Modifier
            .padding(HalfDefault)
            .align(Alignment.CenterStart)
    )
}

@Composable
fun BoxScope.HeaderCell(text: String) {
    Text(
        text = text,
        modifier = Modifier
            .padding(HalfDefault)
            .align(Alignment.CenterStart),
        color = MaterialTheme.colorScheme.primary,
        fontWeight = FontWeight.Bold,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderControlPanel(
    modifier: Modifier = Modifier,
    tempOrderProducts: OrderProducts,
    onCancelClicked: () -> Unit,
    onClearClicked: () -> Unit,
    onPayClicked: () -> Unit,
    onSuspendClicked: () -> Unit,
) {
    Row(
        modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Min),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Spacer(modifier = Modifier.width(HalfDefault))
        Column(
            verticalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.width(IntrinsicSize.Min)
        ) {
            Row(Modifier.height(IntrinsicSize.Max)) {
                ElevatedCard(
                    onClick = onCancelClicked,
                ) {
                    Column(
                        Modifier
                            .aspectRatio(1f)
                            .padding(Default),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Icon(
                            imageVector = Icons.Default.Cancel,
                            contentDescription = "Cancel"
                        )
                        Spacer(modifier = Modifier.padding(HalfDefault))
                        Text(text = "Cancel")
                    }
                }
                Spacer(modifier = Modifier.width(QuarterDefault))
                ElevatedCard(
                    onClick = onClearClicked,
                    modifier = Modifier,
                    enabled = tempOrderProducts.productItems.isNotEmpty()
                ) {
                    Column(
                        Modifier
                            .aspectRatio(1f)
                            .padding(Default),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                    ) {
                        Icon(
                            imageVector = Icons.Default.Clear,
                            contentDescription = "Clear"
                        )
                        Spacer(modifier = Modifier.padding(HalfDefault))
                        Text(text = "Clear")
                    }
                }
            }
            Spacer(modifier = Modifier.height(QuarterDefault))
            ElevatedCard(
                modifier = Modifier
            ) {
                Column(
                    Modifier
                        .padding(Default)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Icon(
                        imageVector = Icons.Default.Print,
                        contentDescription = "New Order"
                    )
                    Spacer(modifier = Modifier.padding(HalfDefault))
                    Text(text = "Print Quotation")
                }
            }
        }
        Spacer(modifier = Modifier.width(QuarterDefault))
        Card(
            Modifier
                .weight(1f)
        ) {
            Spacer(modifier = Modifier.weight(1f))
            Row(
                Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(HalfDefault)
                    .horizontalScroll(rememberScrollState())
            ) {
                Column {
                    Text(text = "Total items", Modifier)
                    Text(text = "Quantity", Modifier)
                    Text(text = "Subtotal", Modifier)
                    Text(text = "Tax", Modifier)
                    Text(text = "Discount", Modifier)
                    Text(text = "Total", Modifier, fontWeight = FontWeight.Bold)
                }
                Spacer(modifier = Modifier.width(HalfDefault))
                Column {
                    Text(text = tempOrderProducts.productsCount.toString(), Modifier)
                    Text(text = tempOrderProducts.totalQuantity.toString(), Modifier)
                    Text(text = tempOrderProducts.subtotalAmount.toStringWith2DigitsWithAED())
                    Text(text = tempOrderProducts.totalTax.toStringWith2DigitsWithAED(), Modifier)
                    Text(text = "0", Modifier)
                    Text(
                        text = tempOrderProducts.totalAmount.toStringWith2DigitsWithAED(),
                        Modifier,
                        fontWeight = FontWeight.Bold
                    )
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                    Spacer(modifier = Modifier.width(QuarterDefault))
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                    Spacer(modifier = Modifier.width(QuarterDefault))
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                    Spacer(modifier = Modifier.width(QuarterDefault))
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                    Spacer(modifier = Modifier.width(QuarterDefault))
                }
                Row(Modifier.height(IntrinsicSize.Min)) {
                    Spacer(modifier = Modifier.width(QuarterDefault))
                }
            }
            Spacer(modifier = Modifier.weight(1f))
        }
        Spacer(modifier = Modifier.width(QuarterDefault))
        Column(
            verticalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.width(IntrinsicSize.Min)
        ) {
            ElevatedCard(
                modifier = Modifier,
                onClick = onPayClicked,
                enabled = tempOrderProducts.productItems.isNotEmpty()
            ) {
                Column(
                    Modifier
                        .padding(Default)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    Icon(
                        imageVector = Icons.Default.Paid,
                        contentDescription = "New Order"
                    )
                    Spacer(modifier = Modifier.padding(HalfDefault))
                    Text(text = "Payment")
                }
            }
            Spacer(modifier = Modifier.height(QuarterDefault))
            Row(Modifier.height(IntrinsicSize.Max)) {
                ElevatedCard(
                    onClick = onSuspendClicked,
                ) {
                    Column(
                        Modifier
                            .aspectRatio(1f)
                            .padding(Default),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Icon(
                            imageVector = Icons.Default.Pause,
                            contentDescription = "Suspend"
                        )
                        Spacer(modifier = Modifier.padding(HalfDefault))
                        Text(text = "Suspend")
                    }
                }
                Spacer(modifier = Modifier.width(QuarterDefault))
                ElevatedCard(
                    onClick = { /*TODO*/ },
                    modifier = Modifier
                ) {
                    Column(
                        Modifier
                            .aspectRatio(1f)
                            .padding(Default),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Icon(
                            imageVector = Icons.Default.Percent,
                            contentDescription = "Discount"
                        )
                        Spacer(modifier = Modifier.padding(HalfDefault))
                        Text(text = "Discount")
                    }
                }
            }
        }
        Spacer(modifier = Modifier.width(HalfDefault))
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductsControlPanel(
    modifier: Modifier = Modifier, products: List<Product>,
    onAddProductClicked: () -> Unit,
    onProductClicked: (product: Product) -> Unit
) {

    Column(modifier) {
        Row {
            IconButton(onClick = onAddProductClicked) {
                Icon(imageVector = Icons.Default.Add, contentDescription = "Add Product")
            }
        }
        LazyVerticalGrid(
            modifier = Modifier.weight(1f), columns = GridCells.Adaptive(100.dp),
            contentPadding = PaddingValues(QuarterDefault),
            verticalArrangement = Arrangement.spacedBy(HalfDefault),
            horizontalArrangement = Arrangement.spacedBy(HalfDefault),
        ) {
            items(products, key = { it.id }) { product ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f),
                    onClick = { onProductClicked(product) }
                ) {
                    Column(
                        Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.SpaceEvenly,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(text = product.name)
                        Text(text = product.price.toStringWith2DigitsWithAED())
                    }
                }
            }
        }
    }
}

@PreviewScreenSizes
@Composable
fun OrderControlPanelPreview() {
    LenadorSystemsTestTheme {
        OrderControlPanel(
            modifier = Modifier,
            tempOrderProducts = OrderProducts(
                order = Order(
                    id = 7394,
                    creationDate = "tota",
                    state = OrderState.Paid,
                    taxType = TaxType.EXCLUSIVE,
                ),
                productItems = listOf(),
            ),
            onCancelClicked = {}, onClearClicked = {},
            onPayClicked = {}, {}
        )
    }
}


@Preview
@Composable
fun NewOrderScreenPreview() {
    LenadorSystemsTestTheme {
//        NewOrderScreen(
//            tempOrder = viewModel.tempOrder,
//            tempProductItems = viewModel.tempProductItems
//        )
    }
}

@Preview
@Composable
fun OrderProductsTablePreview() {
    LenadorSystemsTestTheme {
        OrderProductsTable(
            tempProductItems = emptyList(),
            onDeleteProductClicked = {}
        )
    }
}