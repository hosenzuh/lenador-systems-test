package com.hussein.lenadorsystemstest.ui.home

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddShoppingCart
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hussein.lenadorsystemstest.ui.theme.Default
import com.hussein.lenadorsystemstest.ui.theme.HalfDefault

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    onNewOrderClicked: () -> Unit,
    onAllOrdersClicked: () -> Unit,
) {
    Box(Modifier.fillMaxSize()) {
        var isSettingsVisible by rememberSaveable {
            mutableStateOf(false)
        }
        Row(
            Modifier
                .fillMaxWidth()
                .padding(HalfDefault),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = "Abu Dhabi Arabic Language Centre",
                style = MaterialTheme.typography.titleLarge
            )

            Spacer(modifier = Modifier.weight(1f))

            IconButton(onClick = {
                isSettingsVisible = true
            }) {
                Icon(imageVector = Icons.Default.Settings, contentDescription = "Settings")
            }
        }

        Row(Modifier.align(Alignment.Center)) {
            ElevatedCard(onClick = onNewOrderClicked) {
                Column(
                    Modifier
                        .padding(Default)
                        .width(100.dp)
                        .aspectRatio(1f),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Icon(
                        imageVector = Icons.Default.AddShoppingCart,
                        contentDescription = "New Order"
                    )
                    Spacer(modifier = Modifier.padding(HalfDefault))
                    Text(text = "New Order")
                }
            }
            Spacer(modifier = Modifier.width(Default))
            ElevatedCard(onClick = onAllOrdersClicked) {
                Column(
                    Modifier
                        .padding(Default)
                        .width(100.dp)
                        .aspectRatio(1f),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Icon(
                        imageVector = Icons.Default.ShoppingCart,
                        contentDescription = "Orders"
                    )
                    Spacer(modifier = Modifier.padding(HalfDefault))
                    Text(text = "Orders")
                }
            }
        }
        AnimatedVisibility(visible = isSettingsVisible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            BackHandler {
                isSettingsVisible = false
            }
            Box(
                Modifier
                    .fillMaxSize()
                    .background(Color.LightGray.copy(alpha = 0.5f))
                    .clickable {
                        isSettingsVisible = false
                    }) {
                SettingsDialog {
                    isSettingsVisible = false
                }
            }
        }
    }
}

@Preview
@Composable
private fun HomeScreenPreview() {
    HomeScreen(onNewOrderClicked = {}, onAllOrdersClicked = {})
}