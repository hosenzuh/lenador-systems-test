package com.hussein.lenadorsystemstest.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Print
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.lenadorsystemstest.ui.theme.Default
import com.hussein.lenadorsystemstest.ui.theme.HalfDefault
import com.hussein.lenadorsystemstest.ui.theme.LenadorSystemsTestTheme


@Composable
fun ButtonWithIconAndText(modifier: Modifier = Modifier, icon: ImageVector, text: String) {
    ElevatedCard(
        modifier = modifier
    ) {
        Column(
            Modifier
                .padding(Default)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = icon,
                contentDescription = "content"
            )
            Spacer(modifier = Modifier.padding(HalfDefault))
            Text(text = text)
        }
    }
}

@Preview
@Composable
fun ButtonWithIconAndTextPreview() {
    LenadorSystemsTestTheme {
        ButtonWithIconAndText(icon = Icons.Default.Delete, text = "Delete")
    }
}