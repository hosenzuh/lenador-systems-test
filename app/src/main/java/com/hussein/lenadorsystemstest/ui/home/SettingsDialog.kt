package com.hussein.lenadorsystemstest.ui.home

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.edit
import com.hussein.lenadorsystemstest.ui.theme.Default
import com.hussein.lenadorsystemstest.utils.SHARED_PREFERENCES_KEY
import com.hussein.lenadorsystemstest.utils.TAX_TYPE_KEY
import com.hussein.lenadorsystemstest.utils.TaxType
import com.hussein.lenadorsystemstest.utils.currentTaxType


@Composable
fun BoxScope.SettingsDialog(onConfirmClicked: () -> Unit) {
    ElevatedCard(
        Modifier
            .fillMaxWidth(0.25f)
            .align(Alignment.Center)
    ) {
        Column(
            Modifier
                .align(Alignment.CenterHorizontally),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val context = LocalContext.current
            val sharedPreferences =
                context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE)
            Text(text = "Settings", style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.height(Default))
            Text(text = "Tax Type")
            var isInclusive by rememberSaveable {
                mutableStateOf(sharedPreferences.getBoolean(TAX_TYPE_KEY, false))
            }
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = "Inclusive", Modifier.clickable { isInclusive = true })
                RadioButton(selected = isInclusive, onClick = { isInclusive = true })
            }
            Spacer(modifier = Modifier.height(Default))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = "Exclusive", Modifier.clickable { isInclusive = false })
                RadioButton(selected = !isInclusive, onClick = { isInclusive = false })
            }
            Spacer(modifier = Modifier.height(Default))
            ElevatedButton(onClick = {
                sharedPreferences.edit(true) {
                    putBoolean(TAX_TYPE_KEY, isInclusive)
                }
                currentTaxType = isInclusive.let {
                    if (it) TaxType.INCLUSIVE
                    else TaxType.EXCLUSIVE
                }
                onConfirmClicked()
            }) {
                Text(text = "Confirm")
            }
        }
    }
}

@Preview
@Composable
fun SettingsDialogPreview() {
    Box {
        SettingsDialog({})
    }
}