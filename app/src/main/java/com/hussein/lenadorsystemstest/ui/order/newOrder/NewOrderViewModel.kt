package com.hussein.lenadorsystemstest.ui.order.newOrder

import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import androidx.compose.ui.util.fastMap
import androidx.lifecycle.ViewModel
import com.hussein.lenadorsystemstest.domain.order.AddOrderUseCase
import com.hussein.lenadorsystemstest.domain.product.AddProductUseCase
import com.hussein.lenadorsystemstest.domain.product.GetAllProductsUseCase
import com.hussein.lenadorsystemstest.model.Order
import com.hussein.lenadorsystemstest.model.OrderProducts
import com.hussein.lenadorsystemstest.model.OrderState
import com.hussein.lenadorsystemstest.model.Product
import com.hussein.lenadorsystemstest.model.ProductItem
import com.hussein.lenadorsystemstest.utils.currentTaxType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class NewOrderViewModel @Inject constructor(
    getAllProductsUseCase: GetAllProductsUseCase,
    private val addProductUseCase: AddProductUseCase,
    private val addOrderUseCase: AddOrderUseCase,
) : ViewModel() {

    val tempOrderProducts = MutableStateFlow(
        OrderProducts(
            Order(
                creationDate = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Date()),
                state = OrderState.NONE,
                taxType = currentTaxType
            ),
            emptyList(),
        )
    )

    val productsFlow = getAllProductsUseCase()

    fun addProduct(name: String, price: String) = addProductUseCase(name, price)

    fun addOrder() =
        addOrderUseCase(
            tempOrderProducts.value.order.copy(state = OrderState.Paid),
            tempOrderProducts.value.productItems
        )

    fun suspendOrder() =
        addOrderUseCase(
            tempOrderProducts.value.order.copy(state = OrderState.Suspended),
            tempOrderProducts.value.productItems
        )

    fun onProductClicked(product: Product) {
        tempOrderProducts.update { orderProducts ->

            val isAlreadyExist =
                orderProducts.productItems.find { productItem -> productItem.productId == product.id } != null
            val updatedList = if (isAlreadyExist) {
                orderProducts.productItems.fastMap {
                    if (it.productId == product.id) {
                        it.copy(quantity = it.quantity + 1)
                    } else
                        it
                }
            } else {
                orderProducts.productItems + ProductItem(
                    id = 0,
                    orderId = 0,
                    productId = product.id,
                    name = product.name,
                    price = product.price,
                    quantity = 1,
                    discount = 0.0,
                    currentTaxType
                )
            }

            tempOrderProducts.value.copy(productItems = updatedList)
        }
    }

    fun onDeleteProductClicked(productItem: ProductItem) {
        tempOrderProducts.update { orderProducts ->
            val item =
                orderProducts.productItems.first { it.productId == productItem.productId }
            val updatedList = if (item.quantity > 1) {
                orderProducts.productItems.fastMap {
                    if (it.productId == productItem.productId) {
                        it.copy(quantity = it.quantity - 1)
                    } else
                        it
                }
            } else {
                orderProducts.productItems - item
            }
            tempOrderProducts.value.copy(productItems = updatedList)
        }
    }

    fun onClearClicked() {
        tempOrderProducts.update {
            OrderProducts(
                Order(
                    creationDate = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Date()),
                    state = OrderState.NONE,
                    taxType = currentTaxType
                ),
                emptyList(),
            )
        }
    }
}