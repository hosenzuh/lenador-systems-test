package com.hussein.lenadorsystemstest.ui.order.allOrders

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.lenadorsystemstest.domain.base.State
import com.hussein.lenadorsystemstest.model.OrderProducts
import com.hussein.lenadorsystemstest.ui.order.newOrder.ContentCell
import com.hussein.lenadorsystemstest.ui.order.newOrder.HeaderCell
import com.hussein.lenadorsystemstest.utils.toStringWith2DigitsWithAED
import com.sunnychung.lib.android.composabletable.ux.Table


@Composable
fun AllOrdersScreen(allOrdersState: State<List<OrderProducts>>) {
    val orders = (allOrdersState as? State.Success.Data)?.data ?: emptyList()

    Column(Modifier.fillMaxSize()) {
        Table(
            modifier = Modifier.fillMaxSize(), columnCount = 6, rowCount = orders.size + 1,
        ) { rowIndex, columnIndex ->
            Box {

                when (rowIndex) {
                    0 -> {
                        when (columnIndex) {
                            0 -> HeaderCell(text = "Transaction Id")
                            1 -> HeaderCell(text = "Status")
                            2 -> HeaderCell(text = "Amount")
                            3 -> HeaderCell(text = "Items")
                            4 -> HeaderCell(text = "Quantity")
                            5 -> HeaderCell(text = "Created Date")
                        }
                    }

                    else -> {
                        val rowItem = orders[rowIndex - 1]
                        with(rowItem) {
                            when (columnIndex) {
                                0 -> ContentCell(text = order.id.toString())
                                1 -> ContentCell(text = order.state.toString())
                                2 -> ContentCell(text = totalAmount.toStringWith2DigitsWithAED())
                                3 -> ContentCell(text = productsCount.toString())
                                4 -> ContentCell(text = totalQuantity.toString())
                                5 -> ContentCell(text = order.creationDate)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun AllOrdersScreenPreview() {
    AllOrdersScreen(State.initial())
}