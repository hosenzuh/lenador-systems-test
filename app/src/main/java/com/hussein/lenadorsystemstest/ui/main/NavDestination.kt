package com.hussein.lenadorsystemstest.ui.main

sealed class NavDestination(
    val route: String,
) {

    data object Home : NavDestination("home")
    data object NewOrder : NavDestination("newOrder")
    data object AllOrders : NavDestination("allOrders")
}