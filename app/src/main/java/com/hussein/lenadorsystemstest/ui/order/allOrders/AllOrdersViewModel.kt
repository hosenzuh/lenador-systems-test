package com.hussein.lenadorsystemstest.ui.order.allOrders

import androidx.lifecycle.ViewModel
import com.hussein.lenadorsystemstest.domain.order.GetAllOrdersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AllOrdersViewModel @Inject constructor(
    getAllOrdersUseCase: GetAllOrdersUseCase,
) : ViewModel() {

    val allOrdersFlow = getAllOrdersUseCase()
}